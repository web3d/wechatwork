<?php

namespace chemicle\wechat\work\enum;

/**
 * 消息类型
 */
class MsgType
{
    /**
     * @staticvar string 文本
     */
    const TEXT = 'text';
    
    /**
     * @staticvar string 图片消息
     */
    const IMAGE = 'image';
    
    /**
     * @staticvar string 语音消息
     */
    const VOICE = 'voice';
    
    /**
     * @staticvar string 视频消息
     */
    const VIDEO = 'video';
    
    /**
     * @staticvar string 位置消息
     */
    const LACATION = 'location';
    
    /**
     * @staticvar string 链接消息
     */
    const LINK = 'link';
    
    /**
     * @staticvar string 文件消息 可发送的消息类型
     */
    const FILE = 'file';
    
    /**
     * @staticvar string 文本卡片消息 可发送的消息类型
     */
    const TEXTCARD = 'textcard';
    
    /**
     * @staticvar string 图文消息 可发送的消息类型
     */
    const NEWS = 'news';
    
    /**
     * @staticvar string 独立的图文素材 可发送的消息类型
     * 可以通过管理后台的“管理工具” - “消息群发助手”来发送 跟普通的图文消息在客户端表现上是一致的 
     */
    const MPNEWS = 'mpnews';
}

