<?php

namespace chemicle\wechat\work\api;

use chemicle\wechat\work\entity\Base as BaseEntity;
use chemicle\wechat\work\entity\user\Info;
use chemicle\wechat\work\entity\user\Detail;
use chemicle\wechat\work\entity\user\User as UserEntity;

/**
 * 企业用户管理
 */
class User extends Base
{
    
    const URI_CREATE = '/user/create';

    const URI_GET = '/user/get';
    
    const URI_GET_INFO = '/user/getuserinfo';
    
    const URI_GET_DETAIL = '/user/getuserdetail';
    
    const URI_UPDATE = '/user/update';
    
    const URI_DELETE = '/user/delete';
    
    const URI_BATCH_DELETE = '/user/batchdelete';
    
    const URI_SIMPLE_LIST = '/user/simplelist';
    
    const URI_LIST = '/user/list';
    
    const URI_TO_OPENID = '/user/convert_to_openid';
    
    const URI_TO_USERID = '/user/convert_to_userid';
    
    const URI_AUTH = '/user/authsucc';
    
    /**
     * 换取 userid 及 user_ticket 或 openid
     * @param string $code 经过oauth2授权返回的code
     * @return Info
     * @link https://work.weixin.qq.com/api/doc#10028/根据code获取成员信息
     */
    public function getId($code)
    {        
        $result = $this->get(static::URI_GET_INFO, ['code' => $code]);
        
        return $result;
    }
    
    /**
     * 使用user_ticket获取成员详情
     * @param string $userTicket
     * @return Detail
     * @link https://work.weixin.qq.com/api/doc#10028/使用user_ticket获取成员详情
     */
    public function getDetail($userTicket)
    {
        $result = $this->post(static::URI_GET_DETAIL, ['user_ticket' => $userTicket]);
        
        return $result;
    }
    
    /**
     * 创建成员
     * @param UserEntity $user
     * @return BaseEntity
     * @link https://work.weixin.qq.com/api/doc#10018
     */
    public function create(UserEntity $user)
    {
        return $this->post(static::URI_CREATE, (array) $user);
    }
    
    /**
     * 更新成员
     * @param UserEntity $user
     * @return BaseEntity
     * @link https://work.weixin.qq.com/api/doc#10020
     */
    public function update(UserEntity $user)
    {
        return $this->post(static::URI_UPDATE, (array) $user);
    }
    
    /**
     * 删除成员
     * @param string $userId
     * @return BaseEntity
     * @link https://work.weixin.qq.com/api/doc#10030
     */
    public function delete($userId)
    {
        return $this->get(static::URI_DELETE, ['userid' => $userId]);
    }
    
    /**
     * 批量删除成员
     * @param string $userIds
     * @return BaseEntity
     * @link https://work.weixin.qq.com/api/doc#10060
     */
    public function deleteAll($userIds)
    {
        return $this->post(static::URI_BATCH_DELETE, ['useridlist' => $userIds]);
    }
    
    /**
     * 读取通讯录成员
     * 在通讯录同步助手中此接口可以读取企业通讯录的所有成员信息，而企业自定义的应用可以读取该应用设置的可见范围内的成员信息。
     * @param string $userId
     * @return UserEntity
     * @link https://work.weixin.qq.com/api/doc#10019
     */
    public function one($userId)
    {
        return $this->get(static::URI_GET, ['userid' => $userId]);
    }
    
    /**
     * 获取部门成员详情列表
     * @param int $departmentId 获取的部门id
     * @param int $fetchChild 1/0：是否递归获取子部门下面的成员
     * @return BaseEntity 实际结构是 对象的userlist属性里包含  User 列表
     * @link https://work.weixin.qq.com/api/doc#10063
     */
    public function all($departmentId, $fetchChild = 0)
    {
        return $this->get(static::URI_LIST, ['department_id' => $departmentId, 'fetch_child' => $fetchChild]);
    }
    
    /**
     * 获取部门成员列表
     * @param int $departmentId 获取的部门id
     * @param int $fetchChild 1/0：是否递归获取子部门下面的成员
     * @return BaseEntity 实际结构是 对象的userlist属性里包含 => Detail 但只返回 userid name department三个字段
     * @link https://work.weixin.qq.com/api/doc#10061
     */
    public function allSimply($departmentId, $fetchChild = 0)
    {
        return $this->get(static::URI_SIMPLE_LIST, ['department_id' => $departmentId, 'fetch_child' => $fetchChild]);
    }
    
    /**
     * 该接口使用场景为微信支付、微信红包和企业转账，企业号用户在使用微信支付的功能时，需要自行将企业号的userid转成openid。
     * 在使用微信红包功能时，需要将应用id和userid转成appid和openid才能使用。
     * @param string $userId 企业内的成员id 若有传参agentid，则是针对该agentid的openid。否则是针对企业号corpid的openid
     * @param int $agentid 整型，需要发送红包的应用ID，若只是使用微信支付和企业转账，则无需该参数 非必填
     * @return BaseEntity 附加 openid 和 appid两个属性
     * @link https://work.weixin.qq.com/api/doc#11279
     */
    public function userId2OpenId($userId, $agentid = 0)
    {
        return $this->post(static::URI_TO_OPENID, ['userid' => $userId, 'agentid' => $agentid]);
    }
    
    /**
     * 该接口主要应用于使用微信支付、微信红包和企业转账之后的结果查询，
     * 开发者需要知道某个结果事件的openid对应企业号内成员的信息时，可以通过调用该接口进行转换查询
     * @param string $openId
     * @return BaseEntity 附加 userid 属性
     * @link https://work.weixin.qq.com/api/doc#11279
     */
    public function openId2UserId($openId)
    {
        return $this->post(static::URI_TO_USERID, ['openid' => $openId]);
    }
    
    /**
     * 此接口可以满足安全性要求高的企业进行设置，设置后用户进入企业时需要跳转企业自定义的页面进行验证。
     * 企业在开启时，必须在管理端填写企业二次验证页面的url。
     * 当成员登录企业微信或关注微信插件加入企业时，会自动跳转到企业的验证页面。在跳转到企业的验证页面时，会带上如下参数：code=CODE。
     * 企业可以使用“通讯录同步助手”调用“根据code获取成员信息”接口获取成员的userid。
     * 企业在成员验证成功后，调用该接口即可让成员加入成功。
     * @param string $userId
     * @return BaseEntity
     * @link https://work.weixin.qq.com/api/doc#11378
     */
    public function auth($userId)
    {
        return $this->get(static::URI_AUTH, ['userid' => $userId]);
    }

}
