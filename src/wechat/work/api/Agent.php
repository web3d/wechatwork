<?php

namespace chemicle\wechat\work\api;

use chemicle\wechat\work\entity\Base as BaseEntity;
use chemicle\wechat\work\entity\Agent as AgentEntity;

/**
 * 企业应用相关接口
 */
class Agent extends Base
{
    const URI_GET = '/agent/get';
    const URI_SET = '/agent/set';
    const URI_LIST = '/agent/list';
    
    /**
     * 获取应用列表
     * @return BaseEntity 附加的agentlist属性中存放Agent(agentid,name,square_logo_url)列表
     * @link https://work.weixin.qq.com/api/doc#11214
     */
    public function all()
    {
        return $this->get(static::URI_LIST);
    }
    
    /**
     * 获取应用
     * @param int $id 授权方应用id
     * @return AgentEntity
     */
    public function one($id)
    {
        return $this->get(static::URI_GET, ['agentid' => $id]);
    }
    
    /**
     * 设置应用
     * @param AgentEntity $agent
     * @return BaseEntity
     * @link https://work.weixin.qq.com/api/doc#10088
     */
    public function set(AgentEntity $agent)
    {
        return $this->get(static::URI_SET, (array) $agent);
    }
}

