<?php

namespace chemicle\wechat\work\api;

use chemicle\wechat\work\entity\Base as BaseEntity;

/**
 * 临时素材接口
 */
class Media extends Base
{
    const URI_UPLOAD = '/media/upload';

    const URL_GET = '/media/get';
    
    /**
     * 上传临时素材文件
     * 所有文件size必须大于5个字节
     *      图片（image）：2MB，支持JPG,PNG格式
     *      语音（voice） ：2MB，播放长度不超过60s，支持AMR格式
     *      视频（video） ：10MB，支持MP4格式
     *      普通文件（file）：20MB
     * @param string $type 媒体文件类型，分别有图片（image）、语音（voice）、视频（video），普通文件(file)
     * @param array $media form-data中媒体文件标识，有filename、filelength、content-type等信息
     * @return BaseEntity 附加 type media_id created_at字段
     */
    public function upload($type, $media)
    {
        return $this->post(static::URI_UPLOAD, ['media' => $media], true, ['type' => $type]);
    }
    
    /**
     * 
     * @param type $mediaId
     * @return BaseEntity 错误时返回 BaseEntity; 正确时和普通的http下载相同，请根据http头做相应的处理
     */
    public function one($mediaId)
    {
        return $this->get(static::URL_GET, ['media_id' => $mediaId]);
    }

}
