<?php

namespace chemicle\wechat\work\api;

use chemicle\wechat\work\entity\Base as BaseEntity;

/**
 * 第三方服务商访问一些数据的接口
 * @link https://work.weixin.qq.com/api/doc#10968 第三方服务商相关描述
 * 
 * 
 */
class Service extends Base
{

    const URI_USER_INFO = '/service/get_login_info';
    const URI_PROVIDER_TOKEN = '/service/get_provider_token';

    // =========== 第三方服务商拥有自己的API集合，主要用于完成授权流程以及实现授权后的相关功能 =======

    /**
     * 获取应用套件凭证
     */
    const URI_SUITE_TOKEN = '/service/get_suite_token';

    /**
     * 获取预授权码
     */
    const URI_PRE_AUTH_CODE = '/service/get_pre_auth_code';

    /**
     * 设置授权配置
     */
    const URI_SESSION_INFO = '/service/set_session_info';

    /**
     * 获取企业的永久授权码
     */
    const URI_PERMANENT_CODE = '/service/get_permanent_code';

    /**
     * 获取企业access_token
     */
    const URI_CORP_TOKEN = '/service/get_corp_token';

    /**
     * 获取企业授权信息
     */
    const URI_AUTH_INFO = '/service/get_auth_info';

    /**
     * 根据注册模板生成注册码
     */
    const URI_GEN_REGISTER_CODE = '/service/get_register_code';

    /**
     * 查询注册状态
     */
    const URI_GET_REGISTER_INFO = '/service/get_register_info';

    // ============= end =========

    /**
     * 
     * @param string $suiteId 应用套件id
     * @param string $suiteSecret 应用套件secret
     * @param string $suiteTicket 企业微信后台推送的ticket 并每十分钟更新
     * @return BaseEntity 附加 suite_access_token、expires_in 属性
     * @link https://work.weixin.qq.com/api/doc#10975/获取应用套件凭证
     */
    public function getSuiteAccessToken($suiteId, $suiteSecret, $suiteTicket)
    {
        return $this->post(static::URI_SUITE_TOKEN, [
            'suite_id' => $suiteId, 'suite_secret' => $suiteSecret, 'suite_ticket' => $suiteTicket
        ], false, [], false);
    }

    /**
     * 获取预授权码
     * @param string $suiteAccessToken 应用套件access_token,最长为512字节
     * @param string $suiteId 应用套件id
     * @return BaseEntity 附加 pre_auth_code、expires_in 属性
     * @link https://work.weixin.qq.com/api/doc#10975/获取预授权码
     */
    public function getPreAuthCode($suiteAccessToken, $suiteId)
    {
        return $this->post(static::URI_PRE_AUTH_CODE, ['suite_id' => $suiteId], false, 
                ['suite_access_token' => $suiteAccessToken], false);
    }

    /**
     * 设置授权配置 可设置哪些应用可以授权，不调用则默认允许所有应用进行授权
     * @param string $suiteAccessToken 应用套件access_token
     * @param string $preAuthCode 预授权码
     * @param array $appIds 允许进行授权的应用id，如1、2、3， 不填或者填空数组都表示允许授权套件内所有应用
     * @param int $authType 授权类型：0 正式授权， 1 测试授权， 默认值为0
     * @return BaseEntity
     * @link https://work.weixin.qq.com/api/doc#10975/设置授权配置
     */
    public function setSessionInfo($suiteAccessToken, $preAuthCode, $appIds = [], $authType = 0)
    {
        return $this->post(static::URI_SESSION_INFO, 
                ['pre_auth_code' => $preAuthCode, 'session_info' => ['appid' => $appIds, 'auth_type' => $authType]], 
                false, ['suite_access_token' => $suiteAccessToken], false);
    }

    /**
     * 获取企业永久授权码
     * 使用临时授权码换取授权方的永久授权码，并换取授权信息、企业access_token
     * @link https://work.weixin.qq.com/api/doc#10975/获取企业永久授权码 
     * @param string $suiteAccessToken
     * @param string $suiteId
     * @param string $authCode 临时授权码一次有效 
     *              临时授权码，会在授权成功时附加在redirect_uri中跳转回第三方服务商网站，或通过回调推送给服务商。长度为64至512个字节
     * @return BaseEntity 建议第三方优先以userid或手机号为主键、其次以email为主键来建立自己的管理员账号
     * @link https://work.weixin.qq.com/api/doc#10975/获取企业永久授权码
     */
    public function getPermanentCode($suiteAccessToken, $suiteId, $authCode)
    {
        return $this->post(static::URI_PERMANENT_CODE, ['suite_id' => $suiteId, 'auth_code' => $authCode], 
                false, ['suite_access_token' => $suiteAccessToken], false);
    }

    /**
     * 获取企业授权信息 通过永久授权码换取企业微信的授权信息
     * @param string $suiteAccessToken
     * @param string $suiteId 应用套件id
     * @param string $authCorpId 授权方corpid
     * @param string $permanentCode 永久授权码，通过get_permanent_code获取
     * @return BaseEntity auth_corp_info、auth_info
     * @link https://work.weixin.qq.com/api/doc#10975/获取企业授权信息
     */
    public function getAuthInfo($suiteAccessToken, $suiteId, $authCorpId, $permanentCode)
    {
        return $this->post(static::URI_AUTH_INFO, 
                ['suite_id' => $suiteId, 'auth_corpid' => $authCorpId, 'permanent_code' => $permanentCode], 
                false, ['suite_access_token' => $suiteAccessToken], false);
    }

    /**
     * 获取企业access_token
     * 第三方服务商在取得企业的永久授权码并完成对企业应用的设置之后，便可以开始通过调用企业接口来运营这些应用。
     * 这便是调用企业接口所需的access_token的获取方法
     * @param string $suiteAccessToken
     * @param string $suiteId
     * @param string $authCorpId
     * @param string $permanentCode
     * @return BaseEntity access_token、expires_in
     * @link https://work.weixin.qq.com/api/doc#10975/获取企业access_token
     */
    public function getCorpToken($suiteAccessToken, $suiteId, $authCorpId, $permanentCode)
    {
        return $this->post(static::URI_CORP_TOKEN, 
                ['suite_id' => $suiteId, 'auth_corpid' => $authCorpId, 'permanent_code' => $permanentCode], 
                false, ['suite_access_token' => $suiteAccessToken], false);
    }

    /**
     * 换取服务商的凭证
     * 
     * 代表的是服务商的身份，而与套件或应用无关。请求单点登录、注册定制化等接口需要用到该凭证
     * @param string $corpId 服务商的corpid
     * @param string $providerSecret 服务商的secret，在服务商管理后台可见
     * @return array provider_access_token、expires_in
     * @link https://work.weixin.qq.com/api/doc#11791/服务商的凭证
     */
    public function getProviderAccessToken($corpId, $providerSecret)
    {
        return $this->post(static::URI_PROVIDER_TOKEN, 
                ['corpid' => $corpId, 'provider_secret' => $providerSecret], 
                false, [], false);
    }

    /**
     * 第三方可通过如下接口，获取登录用户的信息。。建议第三方优先以userid为主键、其次以email为主键来匹配账号。
     * 
     * 通过 getProviderAccessToken 方法换取token后,才能调用
     * @param string $providerAccessToken
     * @param string $authCode oauth2.0授权企业微信管理员登录产生的code，最长为512字节。只能使用一次，5分钟未被使用自动过期
     * @return array [usertype, user_info, corp_info, agent, auth_info]
     * @link https://work.weixin.qq.com/api/doc#10991/获取登录用户信息
     */
    public function getUserInfo($providerAccessToken, $authCode)
    {
        return $this->post(static::URI_USER_INFO, 
                ['auth_code' => $authCode], 
                false, ['provider_access_token' => $providerAccessToken], false);
    }

    /**
     * 根据注册模板生成注册码（register_code）
     * @param string $providerAccessToken 应用提供商的provider_access_token
     * @param string $templateId 注册模板id，最长为128个字节
     * @return array register_code、expires_in 注册码，只能消费一次。在访问注册链接时消费。最长为512个字节
     * @link https://work.weixin.qq.com/api/doc#11729/获取注册码
     */
    public function getRegisterCode($providerAccessToken, $templateId)
    {
        return $this->post(static::URI_GEN_REGISTER_CODE, 
                ['template_id' => $templateId], 
                false, ['provider_access_token' => $providerAccessToken], false);
    }

    /**
     * 查询企业注册状态，企业注册成功返回注册信息，若根据register_code查询不到注册信息返回错误码420006。
     * @param string $providerAccessToken 应用提供商的provider_access_token
     * @param string $registerCode 查询的注册码，register_code生成后的查询有效期为24小时
     * @return array corpid,contact_sync[access_token,expires_in]
     * @link https://work.weixin.qq.com/api/doc#11729/查询注册状态
     */
    public function getRegisterInfo($providerAccessToken, $registerCode)
    {
        return $this->post(static::URI_GET_REGISTER_INFO, 
                ['register_code' => $registerCode], 
                false, ['provider_access_token' => $providerAccessToken], false);
    }

}
