<?php

namespace chemicle\wechat\work\api;

use chemicle\wechat\work\entity\Base as BaseEntity;

/**
 * 基础功能接口
 */
class Common extends Base
{
    
    const URI_GET_CALLBACK_IP = '/getcallbackip';
    
    const URI_GET_TOKEN = '/gettoken';
    
    /**
     * 
     * @param string $corpId 企业ID
     * @param string $corpSecret 应用的凭证密钥
     * @return BaseEntity 附加 access_token expires_in
     * @link https://work.weixin.qq.com/api/doc#10013/第三步：获取access_token
     */
    public function getToken($corpId, $corpSecret)
    {
        return $this->get(static::URI_GET_TOKEN, ['corpid' => $corpId, 'corpsecret' => $corpSecret], false);
    }
    
    public function getCallbackIP()
    {
        
    }
}

