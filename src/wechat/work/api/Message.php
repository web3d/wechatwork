<?php

namespace chemicle\wechat\work\api;

use chemicle\wechat\work\api\Base as BaseApi;
use chemicle\wechat\work\entity\message\Base as MessageEntity;

/**
 * 企业微信相关接口
 */
class Message extends BaseApi
{
    const URI_SEND = '/message/send';
    
    /**
     * 应用支持推送文本、图片、视频、文件、图文等类型
     * @param MessageEntity $message
     */
    public function send(MessageEntity $message)
    {
        return $this->post(static::URI_SEND, (array) $message);
    }
}
