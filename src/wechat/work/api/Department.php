<?php

namespace chemicle\wechat\work\api;

use chemicle\wechat\work\entity\Base as BaseEntity;
use chemicle\wechat\work\entity\Department as DepartmentEntity;

/**
 * 企业部门管理
 */
class Department
{
    const URI_CREATE = '/department/create';
    
    const URI_UPDATE = '/department/update';
    
    const URI_DELETE = '/department/delete';
    
    const URI_LIST = '/department/list';
    
    /**
     * 创建部门
     * @param Department $department
     * @return BaseEntity 附加 id 属性
     * @link https://work.weixin.qq.com/api/doc#10076
     */
    public function create(DepartmentEntity $department)
    {
        return $this->post(static::URI_CREATE, (array) $department);
    }
    
    /**
     * 更新部门
     * @param Department $department
     * @return BaseEntity
     * @link https://work.weixin.qq.com/api/doc#10077
     */
    public function update(DepartmentEntity $department)
    {
        return $this->post(static::URI_UPDATE, (array) $department);
    }
    
    /**
     * 删除部门
     * @param int $id
     * @return BaseEntity
     * @link https://work.weixin.qq.com/api/doc#10079
     */
    public function delete($id)
    {
        return $this->get(static::URI_DELETE, ['id' => $id]);
    }
    
    /**
     * 获取部门列表
     * @param int $id 部门id。获取指定部门及其下的子部门。 如果不填，默认获取全量组织架构
     * @return BaseEntity 附加在 department 属性下 存放 Department[] 列表
     * @link https://work.weixin.qq.com/api/doc#10093
     */
    public function all($id = 0)
    {
        $params = [];
        if ($id > 0) {
            $params = ['id' => $id];
        }
        
        return $this->get(static::URI_LIST, $params);
    }

}
