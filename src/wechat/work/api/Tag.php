<?php

namespace chemicle\wechat\work\api;

use chemicle\wechat\work\entity\Tag;

/**
 * 企业标签管理
 */
class Tag extends Base
{
    const URI_CREATE = '/tag/create';
    
    const URI_UPDATE = '/tag/update';
    
    const URI_DELETE = '/tag/delete';
    
    const URI_GET_USERS = '/tag/get';
    
    const URI_ADD_USERS = '/tag/addtagusers';
    
    const URI_DELETE_USERS = '/tag/deltagusers';
    
    const URI_LIST = '/tag/list';
    
    /**
     * 获取标签列表
     * @return BaseEntity 附加在 taglist 属性下 存放 Tag[] 列表
     * @link https://work.weixin.qq.com/api/doc#10926
     */
    public function all()
    {
        return $this->get(static::URI_LIST);
    }
    
    /**
     * 创建标签
     * @param Tag $tag
     * @return BaseEntity 附加 tagid 属性
     * @link https://work.weixin.qq.com/api/doc#10915
     */
    public function create(Tag $tag)
    {
        return $this->post(static::URI_CREATE, (array) $tag);
    }
    
    /**
     * 创建标签
     * @param Tag $tag
     * @return BaseEntity
     * @link https://work.weixin.qq.com/api/doc#10919
     */
    public function update(Tag $tag)
    {
        return $this->post(static::URI_UPDATE, (array) $tag);
    }
    
    /**
     * 删除标签
     * @param int $id
     * @return BaseEntity
     * @link https://work.weixin.qq.com/api/doc#10920
     */
    public function delete($id)
    {
        return $this->get(static::URI_DELETE, ['tagid' => $id]);
    }
    
    /**
     * 获取标签成员
     * 无限制，但返回列表仅包含应用可见范围的成员；第三方可获取自己创建的标签及应用可见范围内的标签详情
     * @param int $id 标签ID
     * @return BaseEntity 附加tagname属性,以及在 userlist 属性下 返回 [userid, name] 列表, 及partylist属性
     * @link https://work.weixin.qq.com/api/doc#10921
     */
    public function allUsers($id)
    {   
        return $this->get(static::URI_GET_USERS, ['tagid' => $id]);
    }
    
    /**
     * 增加标签成员
     * 调用者必须是指定标签的创建者；成员属于应用的可见范围。
     * @param int $id 标签ID
     * @param array $userIds 企业成员ID列表，注意：userlist、partylist不能同时为空，单次请求长度不超过1000 每个标签下的节点不能超过3万个。
     * @param array $partyIds 企业部门ID列表，注意：userlist、partylist不能同时为空，单次请求长度不超过100
     * @return BaseEntity 若部分userid、partylist非法，则附加返回invalidlist invalidparty
     * @link https://work.weixin.qq.com/api/doc#10923
     */
    public function addUsers($id, array $userIds = [], array $partyIds = [])
    {
        return $this->post(static::URI_ADD_USERS, ['tagid' => $id, 'userlist' => $userIds, 'partylist' => $partyIds]);
    }
    
    /**
     * 删除标签成员
     * 调用者必须是指定标签的创建者；成员属于应用的可见范围。
     * @param int $id 标签ID
     * @param array $userIds 企业成员ID列表，注意：userlist、partylist不能同时为空
     * @param array $partyIds 企业部门ID列表，注意：userlist、partylist不能同时为空
     * @return BaseEntity 若部分userid、partylist非法，则附加返回invalidlist invalidparty
     * @link https://work.weixin.qq.com/api/doc#10923
     */
    public function deleteUsers($id, array $userIds = [], array $partyIds = [])
    {
        return $this->post(static::URI_DELETE_USERS, ['tagid' => $id, 'userlist' => $userIds, 'partylist' => $partyIds]);
    }

}
