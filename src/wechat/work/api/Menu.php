<?php

namespace chemicle\wechat\work\api;

use chemicle\wechat\work\entity\Base as BaseEntity;
use chemicle\wechat\work\entity\Menu as MenuEntity;

/**
 * 自定义菜单接口
 */
class Menu extends Base
{
    const URI_CREATE = '/menu/create';
    
    const URL_GET = '/menu/get';
    
    const URL_DELETE = '/menu/delete';
    
    /**
     * 创建菜单
     * @param array $buttons 
     * @see MenuEntity
     * @return BaseEntity
     * @link https://work.weixin.qq.com/api/doc#10786
     */
    public function set(array $buttons)
    {
        return $this->post(static::URI_CREATE, ['button' => (array) $buttons]);
    }
    
    /**
     * 获取菜单 应用须设置为接收消息模式
     * @param int $agentId
     * @return BaseEntity 与创建格式相同
     * @link https://work.weixin.qq.com/api/doc#10787
     */
    public function all($agentId)
    {
        return $this->get(static::URL_GET, ['agentid' => $agentId]);
    }
    
    /**
     * 删除菜单
     * @param int $agentId
     * @return BaseEntity
     * @link https://work.weixin.qq.com/api/doc#10788
     */
    public function delete($agentId)
    {
        return $this->get(static::URL_DELETE, ['agentid' => $agentId]);
    }
}
