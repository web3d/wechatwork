<?php

namespace chemicle\wechat\work\oauth2;

use chemicle\wechat\work\api\Base;

/**
 * Web端收钱;
 * 含企业微信环境内的网页授权、及企业微信客户端环境外的网页授权俗称单点登录（含企业微信管理端单点登录、第三方单点登录）
 * 适用于拥有server端的应用授权
 */
class Client extends Base
{
    
    const URI_PREFIX_INNER = 'https://open.weixin.qq.com/connect/oauth2/authorize';
    
    /**
     * 从第三方网站登录
     */
    const URI_PREFIX_3RD = 'https://open.work.weixin.qq.com/wwopen/sso/3rd_qrConnect';
    
    /**
     * 静默授权，可获取成员的基础信息
     */
    const SCOPE_BASE = 'snsapi_base';
    
    /**
     * 静默授权，可获取成员的详细信息，但不包含手机、邮箱
     */
    const SCOPE_USERINFO = 'snsapi_userinfo';
    
    /**
     * 手动授权，可获取成员的详细信息，包含手机、邮箱
     */
    const SCOPE_PRIVATEINFO = 'snsapi_privateinfo';
    
    /**
     * 获取code
     * 
     * 之后即访问 User::getId 接口换取用户标识: user_id、 
     * user_ticket(相对于传统的oauth2流程种的access_token,后续利用该参数可以获取用户信息或敏感信息。)等
     * @param string $appId 企业的CorpID 必填
     * @param string $redirectURI 授权后重定向的回调链接地址，最终会用urlencode对链接进行处理
     * @param string $state 重定向后会带上state参数，企业可以填写a-zA-Z0-9的参数值，长度不可超过128个字节 非必填
     * @param string $scope 范围 企业无限制；第三方使用snsapi_privateinfo的scope时，应用必须有’成员敏感信息授权’的权限。
     * @param string $agentId
     */
    public function authorizeInner($appId, $redirectURI, $state = null, $scope = '', $agentId = null)
    {
        
    }
    
    /**
     * 让用户使用企业微信管理员或成员帐号登录第三方网站，该登录授权基于OAuth2.0协议标准构建
     * 步骤说明:
     * 1、用户进入服务商网站，如www.ABC.com
     * 2、服务商网站引导用户进入登录授权页
     * 3、用户确认并同意授权
     * 4、授权后回调URI，得到授权码和过期时间
     * 5、利用授权码调用企业微信的相关API
     * 
     * @param string $appId 服务商的CorpID 必填
     * @param string $redirectURI 授权登录之后目的跳转网址，需要做urlencode处理。所在域名需要与登录授权域名一致 必填
     * @param string $state 用于企业或服务商自行校验session，防止跨域攻击 非必填
     * @param string $userType 支持登录的类型。admin代表管理员登录（使用微信扫码）,member代表成员登录（使用企业微信扫码），默认为admin 非必填
     */
    public function authorize3rd($appId, $redirectURI, $state = '', $userType = '')
    {
        
    }
}