<?php

namespace chemicle\wechat\work\entity\user;

/**
 * 用户信息
 * /user/get 接口返回的
 */
class User extends Detail
{
    
    /**
     *
     * @var array 部门内的排序值，默认为0。数量必须和department一致，数值越大排序越前面。值范围是[0, 2^32) 
     */
    public $order;
    
    /**
     *
     * @var int 上级字段，标识是否为上级。0 1
     */
    public $isleader;
    
    /**
     *
     * @var string 座机。第三方仅通讯录套件可获取
     */
    public $telephone;
    
    /**
     *
     * @var string 英文名
     */
    public $english_name;
    
    /**
     *
     * @var array 扩展属性，第三方仅通讯录套件可获取
     */
    public $extattr;
    
    /**
     *
     * @var int 激活状态: 1=已激活，2=已禁用，4=未激活。
     *          已激活代表已激活企业微信或已关注微信插件。未激活代表既未激活企业微信又未关注微信插件。
     */
    public $status;
}