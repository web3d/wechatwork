<?php

namespace chemicle\wechat\work\entity\user;

use chemicle\wechat\work\entity\Base;

/**
 * 用户信息
 * /user/getuserinfo 接口返回的
 */
class Info extends Base
{
    
    /**
     *
     * @var string 手机设备号(由企业微信在安装时随机生成，删除重装会改变，升级不受影响)
     */
    public $DeviceId;
    
    // ============= a) 当用户为企业成员时返回示例如下：
    /**
     *
     * @var string 成员UserID
     */
    public $UserId;
    
    /**
     *
     * @var string 成员票据，最大为512字节。
     *              scope为snsapi_userinfo或snsapi_privateinfo，且用户在应用可见范围之内时返回此参数。
     *              后续利用该参数可以获取用户信息或敏感信息。
     */
    public $user_ticket;
    
    /**
     *
     * @var int user_token的有效时间（秒），随user_ticket一起返回
     */
    public $expires_in;
    
    // ============= b) 非企业成员授权时返回示例如下：
    
    /**
     *
     * @var string 非企业成员的标识，对当前企业唯一
     */
    public $OpenId;
}