<?php

namespace chemicle\wechat\work\entity\user;

use chemicle\wechat\work\entity\Base;

/**
 * 用户信息
 * /user/getuserdetail 接口返回的
 */
class Detail extends Base
{
    /**
     *
     * @var string 成员UserID
     */
    public $userid;
    
    /**
     *
     * @var string 成员姓名
     */
    public $name;
    
    /**
     *
     * @var array 成员所属部门id列表
     */
    public $department;
    
    /**
     *
     * @var string 职位信息
     */
    public $position;
    
    /**
     *
     * @var string 成员手机号，仅在用户同意snsapi_privateinfo授权时返回
     */
    public $mobile = '';
    
    /**
     *
     * @var int 性别。0表示未定义，1表示男性，2表示女性
     */
    public $gender;
    
    /**
     *
     * @var string 成员邮箱，仅在用户同意snsapi_privateinfo授权时返回
     */
    public $email = '';
    
    /**
     *
     * @var string 头像url。注：如果要获取小图将url最后的”/0”改成”/64”即可
     */
    public $avatar;
}