<?php

namespace chemicle\wechat\work\entity\message;

use chemicle\wechat\work\enum\MsgType;

/**
 * 消息数据结构体基类
 * 指定发送目标是否可以共存还有待验证;
 */
abstract class Base
{
    /**
     *
     * @var int 企业应用的id。可在应用的设置页面查看
     */
    protected $agentId;

    /**
     *
     * @var array 成员ID列表（消息接收者，最终请求时多个接收者用‘|’分隔，最多支持1000个）。
     */
    protected $toUsers;
    
    /**
     *
     * @var bool 是否向该企业应用的全部成员发送
     */
    protected $toAll = false;

    /**
     *
     * @var array 部门ID列表，最终请求时多个接收者用‘|’分隔，最多支持100个。当toall时忽略本参数
     */
    protected $toParties;
    
    /**
     *
     * @var array 标签ID列表，最终请求时多个接收者用‘|’分隔，最多支持100个。当toall时忽略本参数
     */
    protected $toTags;

    /**
     *
     * @var int 是否是保密消息，0表示否，1表示是，默认0
     */
    protected $safe = 0;
    
    /**
     *
     * @var string 消息类型
     */
    protected $type = MsgType::TEXT;
    
    /**
     * 
     * @param bool $toAll 是否向该企业应用的全部成员发送
     */
    public function setToAll($toAll)
    {
        $this->toAll = (bool) $toAll;
    }
    
    /**
     * 
     * @param array $userIds 成员ID列表 最多支持1000个
     */
    public function setToUser(array $userIds)
    {
        $this->sliceArray($userIds, 1000);
        
        $this->toUsers = $userIds;
    }
    
    /**
     * 
     * @param array $partyIds  部门ID列表 最多支持100个
     */
    public function setToParty(array $partyIds)
    {
        $this->sliceArray($partyIds, 100);
        
        $this->toParties = $partyIds;
    }
    
    /**
     * 
     * @param array $tagIds 标签ID列表 最多支持100个
     */
    public function setToTag(array $tagIds)
    {
        $this->sliceArray($tagIds, 100);
        
        $this->toTags = $tagIds;
    }
    
    /**
     * 设置消息的类型
     * @param int $safe 是否是保密消息，0表示否，1表示是，默认0
     */
    public function setSafe($safe)
    {
        $this->safe = $safe;
    }
    
    /**
     * 
     * @return array 拼接完整数据
     */
    abstract public function toArray();
    
    /**
     * 
     * @return string 返回json格式的数据
     */
    public function toJSON()
    {
        return json_encode($this->toArray());
    }
    
    /**
     * 将数组切片到指定长度
     * @param array $array
     * @param int $length
     */
    protected function sliceArray(&$array, $length)
    {
        if (count($array) > $length) {
            $array = array_slice($array, 0, $length);
        }
    }
    
    /**
     * 
     * @return array 待输出数据的主体结构
     */
    protected function dataStructure()
    {
        return [
            'agentid' => $this->agentId,
            'touser' => $this->toAll ? '@all' : implode('|', $this->toUsers),
            'totag' => $this->toAll ? '' : implode('|', $this->toTags),
            'toparty' => $this->toAll ? '' : implode('|', $this->toParties),
            'msgtype' => $this->type,
            $this->type => [],
            'safe' => $this->safe
        ];
    }
}
