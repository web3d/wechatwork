<?php

namespace chemicle\wechat\work\entity\message;

use chemicle\wechat\work\enum\MsgType;

/**
 * 视频消息数据结构体
 */
class Video extends Image
{
    
    /**
     *
     * @inheritdoc
     */
    protected $type = MsgType::VIDEO;
    
    /**
     *
     * @var string 视频媒体文件id
     */
    protected $mediaId;
    
    /**
     *
     * @var string 视频消息的标题
     */
    protected $title;
    
    /**
     *
     * @var string 视频消息的描述
     */
    protected $description;
    
    /**
     * 设置消息标题
     * @param string $title 视频消息的标题，不超过128个字节，超过会自动截断
     */
    public function setTitle($title)
    {
        $this->title = $title;
    }
    
    /**
     * 设置视频描述
     * @param sring $description 视频消息的描述，不超过512个字节，超过会自动截断
     */
    public function setDescription($description)
    {
        $this->description = $description;
    }

    /**
     * 
     * @inheritdoc
     */
    public function toArray()
    {
        $data = $this->dataStructure();
        $data[$this->type] = [
            'media_id' => $this->mediaId,
            'title' => $this->title,
            'description' => $this->description
        ];
        
        return $data;
    }
}