<?php

namespace chemicle\wechat\work\entity\message;

use chemicle\wechat\work\enum\MsgType;

/**
 * 语音消息数据结构体
 */
class Voice extends Image
{
    
    /**
     *
     * @inheritdoc
     */
    protected $type = MsgType::VOICE;
    
    /**
     *
     * @var string 语音文件id，可以调用上传临时素材接口获取
     */
    protected $mediaId;

}
