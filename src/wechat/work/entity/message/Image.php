<?php

namespace chemicle\wechat\work\entity\message;

use chemicle\wechat\work\enum\MsgType;

/**
 * 图片消息数据结构体
 */
class Image extends Base
{
    
    /**
     *
     * @inheritdoc
     */
    protected $type = MsgType::IMAGE;
    
    /**
     *
     * @var string 图片媒体文件Id，可以调用上传临时素材接口获取
     */
    protected $mediaId;
    
    /**
     * 
     * @param string $mediaId 文件Id，先调用上传临时素材接口,成功后获取id
     */
    public function setMediaId($mediaId)
    {
        $this->mediaId = $mediaId;
    }

    /**
     * 
     * @inheritdoc
     */
    public function toArray()
    {
        $data = $this->dataStructure();
        $data[$this->type] = [
            'media_id' => $this->mediaId
        ];
        
        return $data;
    }
}