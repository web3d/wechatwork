<?php

namespace chemicle\wechat\work\entity\message;

use chemicle\wechat\work\enum\MsgType;

/**
 * 图文素材消息数据结构体
 */
class MPNews extends Base
{
    /**
     *
     * @inheritdoc
     */
    protected $type = MsgType::MPNEWS;
    
    /**
     *
     * @var array 图文消息，一个图文消息支持1到8条图文
     */
    protected $articles = [];
    
    /**
     * 
     * @param MPNewsArticle $article
     */
    public function addArticle(MPNewsArticle $article)
    {
        $this->articles[] = $article;
    }
    
    /**
     * 
     * @inheritdoc
     */
    public function toArray()
    {
        $data = $this->dataStructure();
        
        $articles = [];
        foreach ($this->articles as $article /** @var MPNewsArticle $article */) {
            $articles[] = [
                'title' => $article->title,
                'content' => $article->content,
                'digest' => $article->digest,
                'content_source_url' => $article->contentSourceUrl,
                'thumb_media_id' => $article->thumbMediaId,
                'author' => $article->author,
            ];
        }
        $data[$this->type] = [
            'articles' => $articles
        ];
        
        return $data;
    }
}