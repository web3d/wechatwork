<?php

namespace chemicle\wechat\work\entity\message;

use chemicle\wechat\work\enum\MsgType;

/**
 * 文本消息数据结构体
 */
class Text extends Base
{
    /**
     *
     * @inheritdoc
     */
    protected $type = MsgType::TEXT;
    
    /**
     *
     * @var string 消息内容，最长不超过2048个字节
     */
    protected $content = '';
    
    /**
     * 支持换行、以及A标签，即可打开自定义的网页 注意：换行符请用转义过的\n
     * @param string $content
     */
    public function setContent($content)
    {
        $this->content = $content;
    }
    
    /**
     * 
     * @inheritdoc
     */
    public function toArray()
    {
        $data = $this->dataStructure();
        $data[$this->type] = [
            'content' => $this->content
        ];
        
        return $data;
    }
}