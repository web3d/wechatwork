<?php

namespace chemicle\wechat\work\entity\message;

use chemicle\wechat\work\enum\MsgType;

/**
 * 图文消息数据结构体
 */
class News extends Base
{
    /**
     *
     * @inheritdoc
     */
    protected $type = MsgType::NEWS;
    
    /**
     *
     * @var array 图文消息，一个图文消息支持1到8条图文
     */
    protected $articles = [];
    
    /**
     * 
     * @param NewsArticle $article
     */
    public function addArticle(NewsArticle $article)
    {
        $this->articles[] = $article;
    }
    
    /**
     * 
     * @inheritdoc
     */
    public function toArray()
    {
        $data = $this->dataStructure();
        
        $articles = [];
        foreach ($this->articles as $article /** @var NewsArticle $article */) {
            $articles[] = [
                'title' => $article->title,
                'description' => $article->description,
                'url' => $article->url,
                'picurl' => $article->picUrl,
                'btntxt' => $article->btnText,
            ];
        }
        $data[$this->type] = [
            'articles' => $articles
        ];
        
        return $data;
    }
}