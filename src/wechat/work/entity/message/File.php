<?php

namespace chemicle\wechat\work\entity\message;

use chemicle\wechat\work\enum\MsgType;

/**
 * File消息数据结构体
 */
class File extends Image
{
    
    /**
     *
     * @inheritdoc
     */
    protected $type = MsgType::FILE;
    
    /**
     *
     * @var string 文件Id，可以调用上传临时素材接口获取
     */
    protected $mediaId;
}