<?php

namespace chemicle\wechat\work\entity\message;

use chemicle\wechat\work\enum\MsgType;

/**
 * 文本卡片消息数据结构体
 */
class TextCard extends Base
{
    
    /**
     *
     * @inheritdoc
     */
    protected $type = MsgType::TEXTCARD;
    
    /**
     *
     * @var string 视频消息的标题
     */
    protected $title;
    
    /**
     *
     * @var string 视频消息的描述
     */
    protected $description;
    
    /**
     *
     * @var string 点击后跳转的链接
     */
    protected $url;

    /**
     *
     * @var string 按钮文字  非必填
     */
    protected $btnText;

    /**
     * 设置消息标题
     * @param string $title 视频消息的标题，不超过128个字节，超过会自动截断
     */
    public function setTitle($title)
    {
        $this->title = $title;
    }
    
    /**
     * 设置视频描述  展现形式非常灵活，支持使用br标签或者空格来进行换行处理，也支持使用div标签来使用不同的字体颜色，目前内置了3种文字颜色：灰色(gray)、高亮(highlight)、默认黑色(normal)，将其作为div标签的class属性即可
     * @param sring $description 视频消息的描述，不超过512个字节，超过会自动截断
     */
    public function setDescription($description)
    {
        $this->description = $description;
    }
    
    /**
     * 
     * @param string $url 点击后跳转的链接
     */
    public function setUrl($url)
    {
        $this->url = $url;
    }
    
    /**
     * 
     * @param string $text 按钮文字 非必填 不超过4个文字，超过自动截断。
     */
    public function setBtnText($text)
    {
        $this->btnText = $text;
    }

    /**
     * 
     * @inheritdoc
     */
    public function toArray()
    {
        $data = $this->dataStructure();
        $data[$this->type] = [
            'title' => $this->title,
            'description' => $this->description,
            'url' => $this->url,
            'btntxt' => $this->btnText
        ];
        
        return $data;
    }
}