<?php

namespace chemicle\wechat\work\entity;

/**
 * 部门信息
 * 
 * 部门的最大层级为15层；部门总数不能超过3万个；每个部门下的节点不能超过3万个。
 */
class Department extends Base
{
    /**
     *
     * @var int 部门id
     */
    public $id;
    
    /**
     *
     * @var string 部门名称 长度限制为1~64个字节，字符不能包括\:?”<>｜
     */
    public $name;
    
    /**
     *
     * @var int 父亲部门id。根部门为1 
     */
    public $parentid;
    
    /**
     *
     * @var int 在父部门中的次序值。order值大的排序靠前。值范围是[0, 2^32)
     */
    public $order;
}