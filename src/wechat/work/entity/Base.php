<?php

namespace chemicle\wechat\work\entity;

class Base
{
    /**
     *
     * @var string 返回码
     */
    public $errcode;
    
    /**
     *
     * @var string 对返回码的文本描述内容
     */
    public $errmsg;
}