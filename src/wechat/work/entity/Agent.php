<?php

namespace chemicle\wechat\work\entity;

/**
 * 应用信息
 * 
 * 第三方仅可设置被授权且非主页型的应用。
 */
class Agent extends Base
{
    /**
     *
     * @var int 企业应用的id
     */
    public $agentid;
    
    /**
     *
     * @var string 企业应用名称 非必填
     */
    public $name;
    
    /**
     *
     * @var string 企业应用详情 非必填
     */
    public $description;
    
    /**
     *
     * @var int 企业应用是否打开地理位置上报 0：不上报；1：进入会话上报； 非必填
     */
    public $report_location_flag;
    
    /**
     *
     * @var string 企业应用可信域名 非必填
     */
    public $redirect_domain;
    
    /**
     *
     * @var int 是否上报用户进入应用事件。0：不接收；1：接收。 非必填
     */
    public $isreportenter;
    
    /**
     * 
     * @var string 应用主页url。url必须以http或者https开头。 非必填
     */
    public $home_url;
    
    /**
     *
     * @var string 企业应用头像的mediaid，设置应用时专用属性 通过多媒体接口上传图片获得mediaid，上传后会自动裁剪成方形和圆形两个头像 非必填
     */
    public $logo_mediaid;
    
    /**
     *
     * @var string 企业应用方形头像url 接口返回该属性
     */
    public $square_logo_url;
    
    /**
     *
     * @var array 企业应用可见范围（标签）详情接口返回该属性
     */
    public $allow_tags;
    
    /**
     *
     * @var array 企业应用可见范围（部门） 详情接口返回该属性
     */
    public $allow_partys;
    
    /**
     *
     * @var array 企业应用可见范围（人员），其中包括userid 详情接口返回该属性
     */
    public $allow_userinfos;
    
    /**
     *
     * @var int 企业应用是否被禁用 详情接口返回该属性
     */
    public $close;
    
    
}