<?php

namespace chemicle\wechat\work\entity;

/**
 * 菜单信息
 * 
 * 一级菜单数组，个数应为1~3个
 */
class Menu extends Base
{
    /**
     *
     * @var int 菜单的响应动作类型
     */
    public $type;
    
    /**
     *
     * @var string 名称
     */
    public $name;
    
    /**
     *
     * @var string 网页链接，成员点击菜单可打开链接，不超过1024字节 view类型必须
     */
    public $url;
    
    /**
     *
     * @var string 菜单KEY值，用于消息接口推送，不超过128字节 click等类型必须
     */
    public $key;
    
    /**
     *
     * @var array 二级菜单数组，个数应为1~5个
     */
    public $sub_button;
    
}