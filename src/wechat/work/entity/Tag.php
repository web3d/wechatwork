<?php

namespace chemicle\wechat\work\entity;

/**
 * 标签信息
 * 
 * 标签总数不能超过3000个
 */
class Tag extends Base
{
    /**
     *
     * @var int 标签id，非负整型，指定此参数时新增的标签会生成对应的标签id，不指定时则以目前最大的id自增。
     */
    public $tagid;
    
    /**
     *
     * @var string 标签名称，长度限制为32个字（汉字或英文字母），标签名不可与其他标签重名。
     */
    public $tagname;
    
    
}